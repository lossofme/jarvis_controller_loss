#include "jarvis_perception/System.hpp"

// STD
#include <string>


#include <std_msgs/Float64.h>
#include <std_msgs/String.h>






namespace jarvis_perception {

System::System(ros::NodeHandle& nodeHandle)
    : nodeHandle_(nodeHandle)
{
  // if (!readParameters()) {
  //   ROS_ERROR("Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
  //   ros::requestShutdown();
  // }

  //Initialize the actuator thread and launch
  perception_ = new Perception(nodeHandle_, "/camera_rgb_optical_frame", "/base_link");


  ros::AsyncSpinner spinner(4);
  spinner.start();


  // while(ros::ok()) {
  //     sleep(0.2);
  //   }
  


}

System::~System()
{
  delete perception_;
}


void System::SetImg(const sensor_msgs::ImageConstPtr& msg) 
{
  perception_->kinectCallBack(msg);
}
void System::SetPointcloud(const sensor_msgs::PointCloud2& cloud) 
{
  perception_->depthCb(cloud);
}

void System::SelectPointcloud(int x, int y)
{
  perception_->SelectPointcloud(x,y,"object");
}
bool System::readParameters()
{
;
}


} /* namespace */
