#include "jarvis_body_hand/drivebase.hpp"






namespace jarvis_body_hand {

DriveBase::DriveBase(ros::NodeHandle& nodeHandle, ControlInput* controlin, Actuator* actuator, Voice* voice)
: 	nodeHandle_(nodeHandle),
	controlin_ (controlin),
	actuator_(actuator),
	voice_(voice)
{

	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;

	Kv = 6.0;
	Kth = 2.0;
	TURN_RATE_LIMIT = 0.5;
	RC_TURN_RATE = 3.0;
	RC_FORW_RATE = 3.0;

	//Update parameter 
	readParameters();

	WheelOut = std::vector<double>{0,0,0,0};
	bGoalUpdated = false;
	bFINISHED = true;
	path_idx = 0;
	bHoldYaw = false;
	bTF_UPDATED = false;

	//Sets up the random number generator
 	srand(time(0));


	subPath_ = nodeHandle_.subscribe("/move_base_node/TrajectoryPlannerROS/global_plan", 1,
									  &DriveBase::pathCb, this);
	subGoal_ = nodeHandle_.subscribe("/move_base_node/current_goal", 10,
									  &DriveBase::goalCb, this);

	subMap_ = nodeHandle_.subscribe("/map", 10,
									  &DriveBase::mapCb, this);
	

	pubPersonGoal_ = nodeHandle_.advertise<geometry_msgs::PoseStamped>
	      		("/move_base_simple/goal", 10);

	pubNavGoal_ = nodeHandle_.advertise<geometry_msgs::PoseStamped>
	      		("/move_base_simple/goal", 10);

	pubNavGoal2_ = nodeHandle_.advertise<geometry_msgs::PoseStamped>
	      		("/move_base_simple/goal", 10);

	pubNextPathGoal_ = nodeHandle_.advertise<geometry_msgs::PoseStamped>
				("/NextPathGoal", 10);

	pubFilteredMap_ = nodeHandle_.advertise<nav_msgs::OccupancyGrid>
				("/map2", 10);


	pubCurTwistTf_ = nodeHandle_.advertise<geometry_msgs::TwistStamped>
				("/jarvis/global/velocity", 10);

	pubCurTwist_ = nodeHandle_.advertise<geometry_msgs::TwistStamped>
				("/jarvis/local/velocity", 10);

	ROS_INFO("DriveBase: Successful launch");

}

DriveBase::~DriveBase()
{
	delete voice_;
	delete actuator_;
}





void DriveBase::Run() 
{
	static ros::Rate r(10);
	while(ros::ok())
	{
		//TODO check empty file , etc
		if( !STOP_THREAD ) 
		{
			GetInput();

			CheckgoalPerson();
			
			GetCurrentPose();
			Control_system();


			std::vector<double> wheelout;
			wheelout = GetWheelOut();
			actuator_->setWheel(wheelout);

			//Safe area to stop
			if(STOP_THREAD)
			{
				// break;
				while(STOP_THREAD && ros::ok()) {
					PAUSED = true;
					r.sleep();
				}
			}
			PAUSED=false;
		}else
		{
			PAUSED = true;
		}
		r.sleep();
	}
}

bool DriveBase::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}
void DriveBase::RequestReset() {
	REQUEST_RESET = true;
	STOP_THREAD = false;
	
}


void DriveBase::pathCb(const nav_msgs::Path::ConstPtr& msg)
{
	
	if(msg->poses.size() >0 && !bFINISHED)
	{
		PathD = *msg;
		bPathUpdated = true;
		// ROS_INFO_STREAM("Get path information size = " << PathD.poses.size());
	}
}
void DriveBase::goalCb(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
	// if(controlin_->GetHeadMode() ==  HEAD_MANUAL)
	// {
		CurGoal = *msg;
		bGoalUpdated = true;
		bPathUpdated = false;

		// voice_->Talk("Goal recieved");
	// }
}

void DriveBase::mapCb(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
	nav_msgs::OccupancyGrid NewMap;

	NewMap=*msg;

	uint32_t width = msg->info.width;
	uint32_t height = msg->info.height;
	float res = msg->info.resolution;
	double origin[2] = {msg->info.origin.position.x, msg->info.origin.position.y};


	if(controlin_->GetHeadMode() ==  HEAD_FOLLOW_PERSON )
	{
		
		double xy_human[2] = {GoalPerson.pose.position.x-origin[0],
							  GoalPerson.pose.position.y-origin[1]};

		const double allow_r = 0.3;
		double xy_human_min[2] = {xy_human[0]-allow_r, xy_human[1]-allow_r};
		double xy_human_max[2] = {xy_human[0]+allow_r, xy_human[1]+allow_r};

		double n_min = Map_xy_to_n(xy_human_min, width, height, res);
		double n_max = Map_xy_to_n(xy_human_max, width, height, res); 

		for(uint32_t i=n_min; i<n_max; i++)
		{
			double pose[2];
			Map_n_to_xy(i, pose, width, height, res );
			if(pose[0] > xy_human[0]-allow_r &&
			   pose[1] > xy_human[1]-allow_r &&
			   pose[0] < xy_human[0]+allow_r &&
			   pose[1] < xy_human[1]+allow_r)
			{
				NewMap.data[i] = 0;
			}
		}
	}


	pubFilteredMap_.publish(NewMap);

	ROS_INFO_THROTTLE(2,"Drivebase : MAP2 updated");
}

double DriveBase::Map_xy_to_n(const double pos[2], uint32_t w, uint32_t h, float res) 
{

	double n = (int)(pos[0]/res)+(int)(pos[1]/res)*w;
	return n;
}

void DriveBase::Map_n_to_xy(uint32_t n, double pos[2], uint32_t w, uint32_t h, float res) 
{

	
	pos[0] = ( (double)( n%w ) *res );
	pos[1] = ( (double)(floor( ((double) n )/ ((double)w)) ) *res );
}

void DriveBase::CheckgoalPerson()
{


	if(controlin_->GetHeadMode() ==  HEAD_FOLLOW_PERSON )
	{
		

		//For request path to avoid obstacle 

		tf::Vector3 persongoal;
		
		
		if(!actuator_->GetPersonGoal(persongoal))
			return;

		GoalPerson.header.frame_id = "map";
		GoalPerson.pose.position.x = persongoal[0];
		GoalPerson.pose.position.y = persongoal[1];
		GoalPerson.pose.position.z = 0;
		GoalPerson.pose.orientation.w = 1;

		Affine3d rawGoal, repoGoal;
		tf::poseMsgToEigen(GoalPerson.pose, rawGoal);
		tf::poseMsgToEigen(RepositionGoal.pose, repoGoal);
		double distance = (rawGoal.translation()-repoGoal.translation()).norm();

		static ros::Time last_pub = ros::Time::now();
		if(ros::Time::now() - last_pub >  ros::Duration(1.0) && distance > 1.0)
		{
			last_pub = ros::Time::now();
			pubPersonGoal_.publish(GoalPerson);
		}

		
	}
}

void DriveBase::CheckNavgoal()
{
        //if(!bGoalUpdated) return;
	ROS_INFO("Successfully send goal");
        NavGoal.header.frame_id = "map";
		NavGoal.pose.position.x = -0.31;
		NavGoal.pose.position.y = 0.00;
		NavGoal.pose.position.z = 0.0;
		NavGoal.pose.orientation.x = 0;
		NavGoal.pose.orientation.y = 0;
		NavGoal.pose.orientation.z = 0.00;
		NavGoal.pose.orientation.w = 1.00;
		if(NavGoal.pose.position.x == NavGoal.pose.position.x &&
		   NavGoal.pose.position.y == NavGoal.pose.position.y &&
		   NavGoal.pose.orientation.z == NavGoal.pose.orientation.z &&
		   NavGoal.pose.orientation.w == NavGoal.pose.orientation.w	)
		{pubNavGoal_.publish(NavGoal);}
	
	    
}

void DriveBase::CheckNavgoal2()
{
        //if(!bGoalUpdated) return;
	ROS_INFO("Successfully send goal");
        NavGoal2.header.frame_id = "map";
		NavGoal2.pose.position.x = -1.22;
		NavGoal2.pose.position.y = -0.20;
		NavGoal2.pose.position.z = 0.0;
		NavGoal2.pose.orientation.x = 0;
		NavGoal2.pose.orientation.y = 0;
		NavGoal2.pose.orientation.z = -0.71;
		NavGoal2.pose.orientation.w = 0.70;
		if(NavGoal2.pose.position.x == NavGoal2.pose.position.x &&
		   NavGoal2.pose.position.y == NavGoal2.pose.position.y &&
		   NavGoal2.pose.orientation.z == NavGoal2.pose.orientation.z &&
		   NavGoal2.pose.orientation.w == NavGoal2.pose.orientation.w	)
		{pubNavGoal2_.publish(NavGoal2);}
	
	    
}

void DriveBase::GetInput()
{
	DRIVE_MODE = controlin_->GetDriveMode();

	if(controlin_->RC.axes[RUD]!=0){
		Vth =RC_TURN_RATE*controlin_->RC.axes[RUD];
		bHoldYaw = false;
	}else{
		//This will effect only non-manual mode
		Vth = 0;
		bHoldYaw = true;
	}

	if(DRIVE_MODE==RC_MODE)
	{
		Vd[x] =  controlin_->RC.axes[ELE];
		Vd[y] =  controlin_->RC.axes[AIL];
		Vd[z] = 0;
		Vd_norm = RC_FORW_RATE*Vd.norm();
		
		dth = atan2(Vd[y], Vd[x]);

		//We shouldn't hold Yaw in manual mode, since it will depend on mapping.
		bHoldYaw = false;
	}


	// ROS_INFO_STREAM("REC->" << Vd[x] << "\t" << Vd[y] << "\t" << Vd[th]);
}

void DriveBase::CheckGoalUpdate()
{
	//Check new goal arrival, reset potential logic here
	if(bGoalUpdated)
	{
		bFINISHED = false;
		//RESET path_idx
		// path_idx = 0;
		bGoalUpdated = false;
	}
}
void DriveBase::Control_system()
{
	CheckGoalUpdate();
	GetTarget();

	//GOTO TARGET
	if(DRIVE_MODE==WP_MODE)
	{
		Vector3d diff = target.translation()-current.translation();
		Vd_norm = Kv*fabs(( diff ).norm());
		Matrix3d R = current.rotation();
		Vector3d diff_bf = R.inverse() * diff;
		dth = (atan2(diff_bf[y], diff_bf[x]));
		// ROS_INFO_STREAM("Heading -> " << dth << "diffx->" << diff_bf[x] << "diffy->" << diff_bf[y]);
	}

	//Hold yaw angle in WP mode since TF still alive!
	if(bHoldYaw && bTF_UPDATED)
	{
		// ROS_INFO("%.3f",(target.rotation().eulerAngles(0,1,2))(2));
		Vth = Kth*((target.rotation().eulerAngles(0,1,2))(2)
			          	 -(current.rotation().eulerAngles(0,1,2))(2));
	}

	//if bfinished or TF lost, stop this machine asap!
	if(bFINISHED && DRIVE_MODE==WP_MODE || !bTF_UPDATED)
	{
		path_idx = 0 ;
		Vd_norm = 0;
		dth = 0;		
	}

	//Limit the Output
	Vth = constrain(Vth, -TURN_RATE_LIMIT, TURN_RATE_LIMIT);
	Vd_norm = constrain(Vd_norm, -FORW_LIMIT, FORW_LIMIT);

}
std::vector<double> DriveBase::GetWheelOut()
{
	// const double quater_pi = 0.785398;
	// WheelOut[0] =   Vd_norm*cos(dth+quater_pi) - Vth;
	// WheelOut[1] =   Vd_norm*sin(dth+quater_pi) - Vth;
	// WheelOut[2] = -(Vd_norm*sin(dth+quater_pi) + Vth);
	// WheelOut[3] = -(Vd_norm*cos(dth+quater_pi) + Vth);
	// return WheelOut;
	const double R = 0.076;
	const double la = 0.460*0.5;
	const double lb = 0.275*0.5;
	const double lab=la+lb;
	Matrix<double, 4, 3> M;
	M << 1,	-1,	-lab,
		 1, 1,	-lab,
		 1, 1,	 lab,
		 1,	-1,	 lab;

    Vector3d VelD;
    VelD << Vd_norm*cos(dth),
    		Vd_norm*sin(dth),
    		Vth;
    Matrix<double, 4, 1> WheelVecOut = (1/R)*M*VelD;
    WheelOut[0] = WheelVecOut[0];
    WheelOut[1] = WheelVecOut[1];
    WheelOut[2] = -WheelVecOut[2];
    WheelOut[3] = -WheelVecOut[3];
    return WheelOut;

}
bool DriveBase::GetCurrentPose()
{


	
	bTF_UPDATED = false;
	try{
		tf::StampedTransform Stransform;
		listener.lookupTransform("/map", "/base_link", ros::Time(0), Stransform);
		transformTFToEigen(Stransform, current);


		geometry_msgs::TwistStamped cur_twist;
		cur_twist.header.frame_id = "map";
		cur_twist.header.stamp = ros::Time::now();
		listener.lookupTwist("/map", "/base_link", ros::Time(0), ros::Duration(0.1), cur_twist.twist);
		pubCurTwistTf_.publish(cur_twist);


		
		bTF_UPDATED = true;
		// ROS_INFO_STREAM("I get x = " << Stransform.getOrigin().x() << "\t" << Stransform.getOrigin().y());
		// ROS_INFO_STREAM("GET ROS TIME\t" << ros::Time::now() << "\t" << Stransform.stamp_);
		return true;
	}
	catch (tf::TransformException &ex) {
	  voice_->Talk("I dont know where im i.");
	  ROS_WARN_THROTTLE(3, "Cannot get current pose, Please open mapping before using drive_base..");
	  ros::Duration(0.05).sleep();
	  // continue;
	  return false;
	}
}
bool DriveBase::CheckRadius(geometry_msgs::Pose pose, double radius)
{	
	Affine3d next;
	tf::poseMsgToEigen( pose, next );
	double distance = fabs((next.translation()-current.translation()).norm());
	bool checker = (distance>radius ? false:true);
	return	checker;
}
bool DriveBase::GetbFINISHED()
{
	return bFINISHED;
}
void DriveBase::GetTarget()
{
	
	//TODO(MAX) REMOVE juggling beheiveor when the new obstruct is observed

	/*  Check radius goal point is in robot base?
	 *  if yes    -> use that target 
	 *  if no     -> check next path point that outside radius and use that target
	 *  if robot is exactly or small radius of goal then turn on FINISH
	 */

	if(controlin_->GetHeadMode() ==  HEAD_FOLLOW_PERSON )
	{
		bFINISHED = CheckRadius(CurGoal.pose, 1.0);
	}else
	{
		bFINISHED = CheckRadius(CurGoal.pose, 0.05);
	}
	
	
	if(bFINISHED) 
	{
		voice_->Talk("I come to goal now");
		ROS_INFO_THROTTLE(3, "FINISHED ! ready to next command");
		//RESET path_idx
		path_idx = 0;
	}
	
	if(!bFINISHED)
	{
		//Check PathD healthy. It should alay be fresh! if not, it prone to collision.
		static ros::Time last_sudo_pub = ros::Time::now();
		if(ros::Time::now() - PathD.header.stamp > ros::Duration(1.0))
		{
			//Try to random valid point around lastest goal 
			if(ros::Time::now() - last_sudo_pub > ros::Duration(0.5))
			{
				last_sudo_pub=ros::Time::now();
				ROS_INFO("DriveBase: Trying to random point around target to wait path available!!");
				
				double random_radius = 0.1;
				RepositionGoal = CurGoal;
				RepositionGoal.pose.position.x = CurGoal.pose.position.x + random_radius*double(rand())/double(RAND_MAX)-random_radius*0.5; //Random number +-random_radius
				RepositionGoal.pose.position.y = CurGoal.pose.position.y + random_radius*double(rand())/double(RAND_MAX)-random_radius*0.5; //Random number +-random_radius
				pubPersonGoal_.publish(RepositionGoal);

				//Update target to current position to prevent move ulti pathD update properly
				target=current;

				bPathUpdated = false;
			}
			
		}

		// ROS_INFO("bPathUpdated = %d", bPathUpdated);


		if(bPathUpdated)
		{
			//FIND TARGET
			if(!CheckRadius(CurGoal.pose, 0.3) && PathD.poses.size()!=0)
			{


				// ROS_INFO("INDEX SIZE = %d", PathD.poses.size());
				for(int i = 0; i<PathD.poses.size(); i++)
				{
					if(CheckRadius(PathD.poses[i].pose, 0.10) && PathD.poses.size() > i+1)
					{
						path_idx=i;
						// ROS_INFO("%d", path_idx);
					}
				}

				//TODO(MAX) FIX BUG HERE!!!
				
				// ROS_INFO("%d", path_idx);

				//Assign translation
				tf::poseMsgToEigen( PathD.poses[path_idx].pose, target );

				//Assign rotation
				//TODO(MAX) : add choice for path following heading or ROI
				Vector3d diff = target.translation()-current.translation();
				double YawTarget = (atan2(diff[y], diff[x]));
				Quaterniond yawq(cos(YawTarget / 2), 0, 0, sin(YawTarget / 2));
				yawq.normalize();
				target.linear() = yawq.toRotationMatrix();

				path_idx = 0;
				
			}else{
				tf::poseMsgToEigen( CurGoal.pose, target );
			}
		}
		
	}

	if(DRIVE_MODE==RC_MODE)
	{
		target.translation()= current.translation();
		if(!bHoldYaw)
			target.linear() = current.rotation();
	}


	geometry_msgs::PoseStamped PoseTargetOut;
	PoseTargetOut.header.frame_id = "map";
	PoseTargetOut.header.stamp = ros::Time::now();
	tf::poseEigenToMsg(target, PoseTargetOut.pose);
	pubNextPathGoal_.publish(PoseTargetOut);
}
void DriveBase::readParameters()
{
	nodeHandle_.getParam("/RC_FORW_RATE", RC_FORW_RATE);
	nodeHandle_.getParam("/RC_TURN_RATE", RC_TURN_RATE);
	nodeHandle_.getParam("/FORW_LIMIT", FORW_LIMIT);
	nodeHandle_.getParam("/TURN_RATE_LIMIT", TURN_RATE_LIMIT);
	nodeHandle_.getParam("/Kth", Kth);
	nodeHandle_.getParam("/Kv", Kv);
}


} /* namespace */
