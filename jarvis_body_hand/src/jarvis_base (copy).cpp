#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <dynamixel_msgs/JointState.h>
#include <tf/tf.h>

#define PI 3.1415926535897932384626433
#define L1 0.245918  
#define L2 0.137
#define R 0.076



double wheel1,wheel2,wheel3,wheel4;

void wheel0Callback(const dynamixel_msgs::JointState& msg0)
{
  wheel1 = msg0.velocity;
}
void wheel1Callback(const dynamixel_msgs::JointState& msg1)
{
  wheel3 = msg1.velocity;
}
void wheel2Callback(const dynamixel_msgs::JointState& msg2)
{
  wheel2 =-msg2.velocity;
}
void wheel3Callback(const dynamixel_msgs::JointState& msg3)
{
  wheel4 = -msg3.velocity;
}

int main(int argc, char** argv){
  
  ros::init(argc, argv, "jarvis_base");
  ros::NodeHandle n;
  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 2);
  ros::Subscriber sub0 = n.subscribe("wheel0_controller/state", 1000, wheel0Callback);
  ros::Subscriber sub1 = n.subscribe("wheel1_controller/state", 1000, wheel1Callback);
  ros::Subscriber sub2 = n.subscribe("wheel2_controller/state", 1000, wheel2Callback);
  ros::Subscriber sub3 = n.subscribe("wheel3_controller/state", 1000, wheel3Callback);
 
  tf::TransformBroadcaster odom_broadcaster;

  double x=0;
  double y=0;
  double th=0;

  double dt=0;
  double delta_x=0;
  double delta_y=0;
  double delta_th=0;

  double vx=0;
  double vy=0;
  double vth=0;

  ros::Time current_time, last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(20.0);
  while(n.ok()){

    ros::spinOnce();               // check for incoming messages
    current_time = ros::Time::now();

    vx = ((wheel1 + wheel2 + wheel3 + wheel4)*R)/4;
    vy = ((- wheel1 + wheel2 + wheel3 - wheel4)*R)/4;
    vth = ((- wheel1 + wheel2 - wheel3 + wheel4)*R)/(4*(L1 + L2));

    dt = (current_time - last_time).toSec();
    delta_x = (vx * cos(th) - vy * sin(th)) * dt;
    delta_y = (vx * sin(th) + vy * cos(th)) * dt;
    delta_th = vth * dt;

    x += delta_x;
    y += delta_y;
    th += delta_th;
    
    //printf("%.2f\n",th);
    //since all odometry is 6DOF we'll need a quaternion created from yaw
    
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "odom_combined";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;
    
    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "odom_combined";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time = current_time;

    
    r.sleep();
  }
  return 0;
}
