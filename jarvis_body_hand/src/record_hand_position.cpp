#include "jarvis_body_hand/record_hand_position.hpp"
/*
 * Description : Record joints position and playback
 * Author      : Thanabadee Bulunseechart
 */


namespace jarvis_body_hand {

Record_hand::Record_hand(ros::NodeHandle& nodeHandle, Actuator* actuator)
    : nodeHandle_(nodeHandle),
    	actuator_(actuator)
{

	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;

	readParameters();


}

Record_hand::~Record_hand()
{
}


void Record_hand::addState(const dynamixel_msgs::MotorStateList::ConstPtr& motorstates)
{
 //NOTE:(MAX) This motor sequece can conflict due to main controller node didn't 
 //alignment to the joint controller e.g., serie topics align with ID low -> high
 //but non-series topic the motor id will alignment correspond to list defined 
 //in yaml.
 //To handle this issue, we NEED to set id motor to match with arm(right-left)->head->based
 actuator_->setRawMotALL(motorstates);
}

void Record_hand::LoadBag( std::string bag_name )
{

	//Stop wait make sure it pause -> putvalue -> reset=start
	while(!Stop() && ros::ok()) {
		ROS_INFO("Wait for stop Record thread");
		sleep(0.1);
	};
	ROS_INFO("BAG LOADING");
	MotorListMsgQueue.clear();
	ROS_INFO("CLEAR QUEUE OK");

	std::string path = ros::package::getPath("jarvis_body_hand");

	rosbag::Bag bag;
  bag.open(path + "/bag_for_pose/" + bag_name + ".bag", rosbag::bagmode::Read);

  std::vector<std::string> topics;
  topics.push_back(std::string("/motor_states/pan_tilt_port"));

  rosbag::View view(bag, rosbag::TopicQuery(topics));

  foreach(rosbag::MessageInstance const m, view)
  {
      dynamixel_msgs::MotorStateList::ConstPtr s = m.instantiate<dynamixel_msgs::MotorStateList>();
      if (s != NULL)
      	MotorListMsgQueue.push_back(s);
  }

  bag.close();
  ROS_INFO("BAG LOADED..");

  RequestReset();
}

void Record_hand::Run() 
{
	while(ros::ok())
	{
		//TODO check empty file , etc
		if( MotorListMsgQueue.size() > 0 && !STOP_THREAD) 
		{
			dynamixel_msgs::MotorStateList::ConstPtr js_ = MotorListMsgQueue.front();
			static double bag_time = js_->motor_states[0].timestamp;

			if(REQUEST_RESET)
			{
				bag_time = js_->motor_states[0].timestamp;
				startcputime = ros::Time::now().toSec();
				REQUEST_RESET=false;

			}
			
			double cpu_duration = (ros::Time::now().toSec() - startcputime);
			// std::cout << cpu_duration << "s" << std::endl;
			
			if( (js_->motor_states[0].timestamp - bag_time) < cpu_duration ) {
				ROS_INFO_STREAM("ts:" << js_->motor_states[0].timestamp - bag_time);

				addState(js_);

				MotorListMsgQueue.erase(MotorListMsgQueue.begin());
			}
			// std::cout << "Hello world." << std::endl; 

			//Safe area to stop
			if(STOP_THREAD)
			{
				// break;
				while(STOP_THREAD && ros::ok()) {
					PAUSED = true;
					sleep(0.5);
				}
			}
			PAUSED=false;
		}else
		{
			PAUSED = true;
		}
		sleep(0.5);
	}
}

bool Record_hand::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}
void Record_hand::RequestReset() {
	REQUEST_RESET = true;
	STOP_THREAD = false;
	
}
bool Record_hand::FINISH() const
{
	return MotorListMsgQueue.size()==0;
}

void Record_hand::readParameters()
{

}

} /* namespace */
