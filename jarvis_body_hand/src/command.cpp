#include "jarvis_body_hand/command.hpp"

#include <sstream> //needed for my to_string implementation
#include <iostream>


namespace jarvis_body_hand {
using namespace std;
using namespace Eigen;


Command::Command(ros::NodeHandle& nodeHandle, Voice* voice, ControlInput* controlin, Kinematic* kinetic, Actuator* actuator, Record_hand* rec_hand, DriveBase* drivebase)
    : nodeHandle_(nodeHandle),
      voice_(voice),
      controlin_(controlin),
      kinetic_(kinetic),
      actuator_(actuator),
      rec_hand_(rec_hand),
      drivebase_(drivebase)
{

    STOP_THREAD = false;
    REQUEST_RESET = true;
    PAUSED=false;

    readParameters();


    ROS_INFO("CMD: Successful launch\n");
}

Command::~Command()
{
}






void Command::Run() 
{
    while(ros::ok())
    {
        //TODO check empty file , etc
        if( !STOP_THREAD ) 
        {
            //DO SOMETHING
            
            string input;

            getline(cin, input);

            if(input == "list" || input == "-h"){
              Do_list();
            }
            else if(input == ""){
              cout << "\nCMD: ";
            }

            /* Kinetics */
            else if(input == "arm set home"){
              Do_armsethome();
            }
            else if(input == "arm move"){
              Do_armmove();
            }
            else if(input == "get arm angle"){
              Do_getarmangle();
            }


            /* Drivebase */
            else if(input == "drivebase debug 1"){
              drivebase_->setDebug(true);
            }
            else if(input == "drivebase debug 0"){
              drivebase_->setDebug(false);
            }

            /* Actuator */
            else if(input == "actuator debug 1"){
              actuator_->setDebug(true);
            }
            else if(input == "actuator debug 0"){
              actuator_->setDebug(false);
            }

            /* Wheel */
            else if(input == "get wheel velocity"){
              Do_getwheelvelocity();
            }

            /* Head */
            else if(input == "get head angle"){
              Do_getheadangle();
            }

            else{
              printf("CMD: Invalid command!\n");
            }



            //Safe area to stop
            if(STOP_THREAD)
            {
                // break;
                while(STOP_THREAD && ros::ok()) {
                    PAUSED = true;
                    sleep(0.5);
                }
            }
            PAUSED=false;
        }else
        {
            PAUSED = true;
        }
        sleep(0.1);
    }
}


void Command::Do_list()
{
  cout << "\nCMD: list\n";
  cout << "\t\t" << "-h,list : \tlist all available command.\n"
       << "\t\t" << "arm set home : \tcontrol arm to home position.\n"
       << "\t\t" << "arm move : \tcontrol arm to specific position.\n"
       << "\t\t" << "drivebase debug 1/0 : enable/disable drivebase debug\n"
       << "\t\t" << "actuator debug 1/0 : enable/disable actuator debug\n"
       << "\t\t" << "get wheel velocity : debug wheel velocities\n"
       << "\t\t" << "get head angle : debug head angle\n"
       << "\t\t" << "get arm angle : debug arm angle\n"
       << "\n\n";

}
void Command::Do_armsethome()
{
  printf("CMD: arm set home: Doing set home!\n");
  kinetic_->setHome();
}


void Command::Do_armmove()
{

  bool success = false;
  do {
    printf("CMD: arm move: Enter x position : ");
    string x,y,z;
    getline(cin, x);
    printf("CMD: arm move: Enter y position : ");
    getline(cin, y);
    printf("CMD: arm move: Enter z position : ");
    getline(cin, z);

    if(x=="" || y=="" || z=="") {
      printf("CMD: arm move: Invalid x or y or z.\n");return;
    }
    else{
      printf("CMD: arm move: Arm try to move to %.2f, %.2f, %.2f\n", stof(x),stof(y),stof(z));

      success = kinetic_->MoveArmTo(Vector3d(stof(x),stof(y),stof(z)),
                          Vector3d(0,-1.571,0), false);

      printf("CMD: arm move: Enter 'exit' to go back to main menu or\nCommand: Enter to retry move arm.\n");
      string input;
      getline(cin, input);
      if(input=="exit"){
        printf("CMD: arm move: exiting to main menu\n");
        return;
      }
    }
    
  }while(!success);
}


void Command::Do_getwheelvelocity()
{
  while(ros::ok()){
    for(int i =0;i<200;i++) {
      std::vector<double> wheel;
      actuator_->getWheel(wheel);

      if(wheel.size()!=0)
      {
        for(int i =0;i<wheel.size();i++)
        {
          cout << wheel[i] << "\t";
        }
        cout << endl;
      }else{
        cout << "no wheel topic detected. Check dynamixel topic\n";
        break;
      }
      usleep(500000);
    }
    printf("CMD: Enter to continue or type x to exit\n");
    string input;
    getline(cin, input);
    if(input=="x") break;
  }
  
  
}

void Command::Do_getarmangle()
{
  while(ros::ok()){
    for(int i =0;i<200;i++) {
      std::vector<double> arm;
      actuator_->getArm(arm);

      if(arm.size()!=0)
      {
        for(int i =0;i<arm.size();i++)
        {
          cout << arm[i] << "\t";
        }
        cout << endl;
      }else{
        cout << "no arm topic detected. Check dynamixel topic\n";
        break;
      }
      usleep(500000);
    }
    printf("CMD: Enter to continue or type x to exit\n");
    string input;
    getline(cin, input);
    if(input=="x") break;
  }
  
  
}


void Command::Do_getheadangle()
{
  while(ros::ok()){
    for(int i =0;i<200;i++) {
      std::vector<double> head;
      actuator_->getHead(head);

      if(head.size()!=0)
      {
        for(int i =0;i<head.size();i++)
        {
          cout << head[i] << "\t";
        }
        cout << endl;
      }else{
        cout << "no head topic detected. Check dynamixel topic\n";
        break;
      }
      usleep(500000);
    }
    printf("CMD: Enter to continue or type x to exit\n");
    string input;
    getline(cin, input);
    if(input=="x") break;
  }
  
  
}

bool Command::Stop() 
{
    STOP_THREAD=true;
    return PAUSED;
}
void Command::RequestReset() {
    REQUEST_RESET = true;
    STOP_THREAD = false;
    
}

void Command::readParameters()
{
    // nodeHandle_.getParam("/JOINT_N", JOINT_N);
    // nodeHandle_.getParam("/JOINT_SCALE", JOINT_SCALE);
    // nodeHandle_.getParam("/JOINT_RAW_HOME", JOINT_RAW_HOME);
    // nodeHandle_.getParam("/JOINT_START_IDX", JOINT_START_IDX);
    // nodeHandle_.getParam("/JOINT_GROUP", JOINT_GROUP);
    // if(JOINT_RAW_HOME.size() != JOINT_SCALE.size()) 
    // {
    //  ROS_ERROR("RECORD_HAND: Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
 //     ros::requestShutdown();
    //  while(ros::ok());
    // }
}


} /* namespace */
