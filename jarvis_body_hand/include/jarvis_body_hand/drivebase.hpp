#pragma once


/*
 * Description : Wheel based navigation and control system
 * Author      : Thanabadee Bulunseechart
 */



#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>


#include "jarvis_body_hand/control_input.hpp"
#include "jarvis_body_hand/actuator.hpp"
#include "jarvis_body_hand/voice.hpp"

#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>



namespace jarvis_body_hand {
using namespace Eigen;
/*!
 * Class containing the algorithmic part of the package.
 */
class DriveBase
{


 private:

  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;

  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;
  //! ROS topic publisher.
  ros::Subscriber subPath_;
  ros::Subscriber subGoal_;
  ros::Subscriber subMap_;
  
  ros::Publisher pubPersonGoal_;
  ros::Publisher pubNavGoal_;
  ros::Publisher pubNavGoal2_;
  ros::Publisher pubNextPathGoal_;
  ros::Publisher pubFilteredMap_;
  ros::Publisher pubCurTwistTf_;
  ros::Publisher pubCurTwist_;
  double RC_FORW_RATE;
  double RC_TURN_RATE;
  double FORW_LIMIT;
  double TURN_RATE_LIMIT;
  double Kv;
  double Kth;

  ControlInput* controlin_;
  Actuator* actuator_;
  Voice* voice_;

  
  void pathCb(const nav_msgs::Path::ConstPtr& msg);
  void goalCb(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void mapCb(const nav_msgs::OccupancyGrid::ConstPtr& msg);
  

  double Map_xy_to_n(const double pos[2], uint32_t w, uint32_t h, float res);
  void Map_n_to_xy(uint32_t n, double pos[2], uint32_t w, uint32_t h, float res);

  
  void GetInput();
  void CheckGoalUpdate();
  void Control_system();
  bool GetCurrentPose();
  std::vector<double> GetWheelOut();
  bool CheckRadius(geometry_msgs::Pose pose, double radius);
  void GetTarget();

  void readParameters();

  Vector3d Vd;
  double Vd_norm;
  double Vth;
  double dth;
  std::vector<double> WheelOut;
  nav_msgs::Path PathD;
  geometry_msgs::PoseStamped CurGoal;
  bool bGoalUpdated;
  bool bPathUpdated;
  bool bFINISHED;

  tf::TransformListener listener;
  Affine3d target;
  Affine3d current;
  uint8_t DRIVE_MODE;
  bool bHoldYaw;
  int path_idx;
  bool bTF_UPDATED;

  geometry_msgs::PoseStamped RepositionGoal;

  geometry_msgs::PoseStamped GoalPerson;

  geometry_msgs::PoseStamped NavGoal;

  geometry_msgs::PoseStamped NavGoal2;

  nav_msgs::OccupancyGrid mapMsg;

 public:
  void Run();
  bool Stop();
  void RequestReset();

  bool GetbFINISHED();
  void CheckgoalPerson();
  void CheckNavgoal();
  void CheckNavgoal2();
  /*!
   * Constructor.
   */
  DriveBase(ros::NodeHandle& nodeHandle, ControlInput* controlin, Actuator* actuator, Voice* voice);

  /*!
   * Destructor.
   */
  virtual ~DriveBase();





  
};

} /* namespace */
